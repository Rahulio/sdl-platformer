#pragma once

#include <SDL.h>

class Time {
public:
	static double GetDeltaTime() { return deltaTime; }
	static double GetFixedDeltaTime() { return fixedDeltaTime; }

	static void SetFixedFramerate(unsigned int fixedFramerate);
	static void CalculateDeltaTime();

private:
	static double deltaTime;
	static double fixedDeltaTime;

	static Uint64 ticksThisFrame;
	static Uint64 ticksLastFrame;
};
