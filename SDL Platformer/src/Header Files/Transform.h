#pragma once

class Vector2;
class SDL_Rect;

class Transform {
public:
  Transform();
  Transform(Vector2 position, Vector2 scale);
  SDL_Rect GetDestinationRect();

  Vector2* position;
  Vector2* scale;
};
