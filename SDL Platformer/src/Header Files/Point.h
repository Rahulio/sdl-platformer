#pragma once

#include <ostream>

class Vector2;

class Point {
public:
	Point();
	Point(int x, int y);
	Point(int n);

	friend std::ostream& operator<<(std::ostream& out, const Point& p);

	Vector2 ToVector2();

	int x;
	int y;
};