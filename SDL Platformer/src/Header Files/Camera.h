#pragma once

class Transform;

class Camera {
public:
  Camera(float orthographicSize = 5.0f);

  static float GetUnitSize();
  static float orthographicSize;

  Transform* transform;
};
