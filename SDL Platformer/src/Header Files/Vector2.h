#pragma once

#include <ostream>

class Point;

class Vector2 {
public:
	Vector2();
	Vector2(float x, float y);
	Vector2(float n);

	friend std::ostream& operator<<(std::ostream& out, const Vector2& v);

	Point ToPoint();

	float x;
	float y;
};