#include "Game.h"
#include "Time.h"

#include <iostream>

int main(int argc, char* args[]) {
  Game* game = new Game();

  if (game->Initialise("SDL Game", 1280, 720, 0) == -1)
    return -1;

  Time::SetFixedFramerate(30);
  const double fixedDeltaTime = Time::GetFixedDeltaTime();
  double timeSinceLastFixedUpdate = 0;

  while (game->IsRunning()) {
    game->HandleEvents();
    game->Update();
    game->Render();

    while (timeSinceLastFixedUpdate >= fixedDeltaTime * 0.99) {
      game->FixedUpdate();
      timeSinceLastFixedUpdate -= fixedDeltaTime;
      if (timeSinceLastFixedUpdate < 0) timeSinceLastFixedUpdate = 0;
    }

    Time::CalculateDeltaTime();
    timeSinceLastFixedUpdate += Time::GetDeltaTime();
  }

  delete(game);
  std::cout << "Game closed\n";

  return 0;
}