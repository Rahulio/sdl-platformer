#include "Point.h"
#include "Vector2.h"

Point::Point()
{
  x = y = 0;
}

Point::Point(int x, int y)
{
  this->x = x;
  this->y = y;
}

Point::Point(int n)
{
  x = y = n;
}

Vector2 Point::ToVector2()
{
  return Vector2(x, y);
}

std::ostream& operator<<(std::ostream& out, const Point& p)
{
  out << "(" << p.x << ", " << p.y << ")";
  return out;
}
