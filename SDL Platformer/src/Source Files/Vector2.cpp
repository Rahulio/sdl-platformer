#include "Vector2.h"
#include "Point.h"

Vector2::Vector2()
{
  x = y = 0;
}

Vector2::Vector2(float x, float y)
{
  this->x = x;
  this->y = y;
}

Vector2::Vector2(float n)
{
  x = y = n;
}

Point Vector2::ToPoint()
{
  return Point(x, y);
}

std::ostream& operator<<(std::ostream& out, const Vector2& v)
{
  out << "(" << v.x << ", " << v.y << ")";
  return out;
}
