#include "Transform.h"
#include "Vector2.h"
#include <SDL.h>

Transform::Transform() {
  this->position = new Vector2(0, 0);
  this->scale = new Vector2(1, 1);
}

Transform::Transform(Vector2 position, Vector2 scale) {
  this->position = new Vector2(position.x, position.y);
  this->scale = new Vector2(scale.x, scale.y);
}

SDL_Rect Transform::GetDestinationRect() {
  return SDL_Rect{ (int)position->x, (int)position->y, (int)scale->x, (int)scale->y };
}