#include "Time.h"

double Time::deltaTime = 0;
double Time::fixedDeltaTime = 0;
Uint64 Time::ticksThisFrame = 0;
Uint64 Time::ticksLastFrame = 0;

void Time::SetFixedFramerate(unsigned int fixedFramerate) {
	if (fixedDeltaTime == 0) fixedDeltaTime = 1.0 / fixedFramerate;
}

void Time::CalculateDeltaTime()
{
	ticksLastFrame = ticksThisFrame;
	ticksThisFrame = SDL_GetPerformanceCounter();

	if (ticksLastFrame == 0) deltaTime = 0;
	else deltaTime = (double)((ticksThisFrame - ticksLastFrame) / (double)SDL_GetPerformanceFrequency());
}
