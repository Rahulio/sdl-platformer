#include "Camera.h"
#include "Transform.h"
#include "Game.h"
#include <iostream>

float Camera::orthographicSize = 0;

Camera::Camera(float orthographicSize) {
  this->orthographicSize = orthographicSize;
  transform = new Transform();
}

float Camera::GetUnitSize() {
  return Game::HEIGHT / (orthographicSize * 2.0f);
}
