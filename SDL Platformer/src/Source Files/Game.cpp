#include "Game.h"
#include "SDL.h"
#include <iostream>

SDL_Renderer* Game::renderer = nullptr;
unsigned int Game::WIDTH = 0;
unsigned int Game::HEIGHT = 0;

Game::Game() {
  window = nullptr;
  renderer = nullptr;
  isRunning = false;
}

Game::~Game()
{
  SDL_DestroyRenderer(renderer);
  SDL_DestroyWindow(window);
  renderer = nullptr;
  window = nullptr;
  isRunning = false;
}

int Game::Initialise(const char* title, unsigned int width, unsigned int height, bool fullscreen)
{
  if (SDL_Init(SDL_INIT_EVERYTHING) == -1)
  {
    std::cout << "ERROR while initialising SDL!\n";
    return -1;
  }

  window = SDL_CreateWindow(title, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, width, height, 0);
  if (!window)
  {
    std::cout << "ERROR while creating window!\n";
    return -1;
  }

  renderer = SDL_CreateRenderer(window, -1, 0);
  if (!renderer)
  {
    std::cout << "ERROR while creating renderer!\n";
    return -1;
  }

  HEIGHT = height;
  WIDTH = width;

  isRunning = true;
}

void Game::HandleEvents()
{
  //TODO: Extract this to Input Management System
  SDL_Event event;
  SDL_PollEvent(&event);

  switch (event.type) {
  case SDL_QUIT:
    isRunning = false;
    break;
  default:
    break;
  }
}

void Game::Update()
{
  //TODO: Add update code here
}

void Game::FixedUpdate() {
  //TODO: Add fixed update code here
}

void Game::Render()
{
  SDL_SetRenderDrawColor(renderer, 100, 220, 240, 255);
  SDL_RenderClear(renderer);

  // TODO: Add render code here

  SDL_RenderPresent(renderer);
}

void Game::Quit() {
  isRunning = false;
}
